package Classes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class StdNameComparator {
	public static void main(String[] args) {
//		CompareObjects comp = new CompareObjects();
//		Scanner scanner = new Scanner(System.in);
		ArrayList<Contact> list = new ArrayList<Contact>();
		list.add(new Contact("alex", "alex@gmail.com", 213434));
		list.add(new Contact("bane", "alan@gmail.com", 234233));
		list.add(new Contact("alan", "alan@gmail.com", 234233));
		list.add(new Contact("ashwin", "alan@gmail.com", 234233));
		Collections.sort(list, new CompareObjects());
		for (Contact contact : list) {
			System.out.println(contact.name.toString());
		}
	}
}

class CompareObjects implements Comparator {
	
	String name, email;
	int number;
	
	CompareObjects() {}
	
	public void getName(String name) {
		this.name = name;
	}

	@Override
	public int compare(Object o1, Object o2) {
		// TODO Auto-generated method stub
		Contact contact_1 = (Contact)o1;
		Contact contact_2 = (Contact)o2;
		System.out.print(contact_1.name.compareTo(contact_2.name));
		return contact_1.name.compareTo(contact_2.name);
	}
	
}
