package Classes;

public class Contact {
	
	String name;
	String email;
	int number;
	
	Contact(String name, String email, int number) {
		this.name = name;
		this.email = email;
		this.number = number;
	}
	
}
