package Classes;

public class Student {
	
	long rollNo;
	String name;
	
	Student(long rollNo, String name) {
		this.rollNo = rollNo;
		this.name = name;
	}
	
	int marks1 = 0, marks2 = 0;
	
	public int getMarks1() {
		return this.marks1;
	}
	
	public int getMarks2() {
		return this.marks2;
	}
	
	public void setMarks1(int marks1) {
		this.marks1 = marks1;
	}
	
	public void setMarks2(int marks2) {
		this.marks2 = marks2;
	}
	
	public void displayInfo() {
		System.out.println(rollNo);
		System.out.println(name);
		System.out.println(marks1);
		System.out.println(marks2);
	}
	
}
