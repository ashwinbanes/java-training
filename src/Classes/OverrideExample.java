package Classes;

public class OverrideExample {
	public static void main(String[] args) {
		Sports s = new Soccer();
		Sports t = new Tennis();
		s.getNumberOfPlayers();
		t.getNumberOfPlayers();
	}
}
