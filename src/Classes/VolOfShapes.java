package Classes;

import java.util.Scanner;

public class VolOfShapes {
	public VolOfShapes() {
	}

	public static void execute() {
		Scanner sc = new Scanner(System.in);
		Volume vol = new Volume();;
		System.out.println("Select the shape : ");
		String shape = sc.next();
		switch (shape) {
			case "cube":
				double radius = sc.nextDouble();
			    System.out.print(vol.calculateVolume(radius));
			    break;
			case "cuboid":
				double lenght = sc.nextDouble();
				double breath = sc.nextDouble();
				double height = sc.nextDouble();
				System.out.print(vol.calculateVolume(lenght, breath, height));
				break;
			case "cylinder":
				double radiusC = sc.nextDouble();
				double heightC = sc.nextDouble();
				System.out.print(vol.calculateVolume(radiusC, heightC));
				break;
		}
	}
	
	public static void main(String[] args) {
		execute();
	}

}
