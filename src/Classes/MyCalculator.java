package Classes;
import java.util.Scanner;

public class MyCalculator {
	public static void main(String[] args) {
		Calc cal = new Calc();
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int ans = cal.divisor_sum(n);
		System.out.print(ans);
	}
}

class Calc implements AdvancedArithmetic {
	
	@Override
	public int divisor_sum(int n) {
		// TODO Auto-generated method stub
		int result = 0;
		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				result = result + i;
			}
		}
		return result;
	}
	
}
