package Classes;

import java.util.Scanner;

public class StudentRegister {
	
	public StudentRegister() {
		
	}
	
	public static void studentReg() {
		Scanner sc = new Scanner(System.in);
		long reg_no = sc.nextLong();
		String name = sc.next();
		Student std = new Student(reg_no, name);
		int marks1 = 0, marks2 = 0;
		marks1 = sc.nextInt();
		marks2 = sc.nextInt();
		std.setMarks1(marks1);
		std.setMarks2(marks2);
		sc.close();
		std.displayInfo();
	}
	
	public static void main(String[] args) {
		studentReg();
	}
	
}
