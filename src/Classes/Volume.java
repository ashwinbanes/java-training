package Classes;

public class Volume {

	double vol;

	private static final float PI = 3.14f;
	
	Volume() {
	}

	double calculateVolume(double radius) {
		this.vol = radius * radius * radius;
		return this.vol;
	}

	double calculateVolume(double length, double breath, double height) {
		this.vol = length * breath * height;
		return this.vol;
	}

	double calculateVolume(double radius, double height) {
		this.vol = PI * radius * radius * height;
		return this.vol;
	}
	
}
