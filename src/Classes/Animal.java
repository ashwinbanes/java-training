package Classes;

public class Animal {
	
	int numberOfLegs;
	
	Animal() {
		this.numberOfLegs = 2;
	}
	
	public int getNumberOfLegs() {
		return this.numberOfLegs;
	}
	
	public void walk() {
		System.out.println("Method Walk :)");
	}
	
}
