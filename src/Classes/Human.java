package Classes;

public class Human extends Animal {
	
	Human() {
		
	}
	
	public void speak() {
		System.out.println("Method Speak :)");
	}
	
	public static void main(String[] args) {
		 Human human = new Human();
		 System.out.println(human.getNumberOfLegs());
		 human.walk();
		 human.speak();
	}
	
}
