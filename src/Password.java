import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Password {
	Password() {
		check();
	}
//	private static final String PATTERN_STRING =
//			 " (\r\n" + 
//			 "(?=.*\\d)\r\n" + 
//			 "(?=.*[a-z])\r\n" + 
//			 "(?=.*[A-Z])\r\n" + 
//			 "(?=.*[-!@_$])\r\n" + 
//			 ".\r\n" +
//			 ") ";
	//- ! @ _ $
	private static final String PATTERN_STRING = "^"
			+ "(?=.{2,}[a-z])"
			+ "(?=.{2,}[A-Z])"
			+ "(?=.{1,}[0-9])"
			+ "(?=.*[-!@$])"
			+ "."
			+ "{8,15}"
			+ "$";
//	private static final String PATTERN_STRING_2 = "^"
//			+ "(?=.{2,}[a-z][A-Z]"
//			+ "(?=.{1,}[0-9])"
//			+ "."
//			+ ""
//	private static final String PATTERN_STRING_3 =
	
	public static void main(String[] args) {
		check();
	}
	
	public static void check() {
		Scanner scanner = new Scanner(System.in);
		String str = scanner.next();
		Pattern pattern = Pattern
				.compile(PATTERN_STRING);
		Matcher matcher = pattern.matcher(str);
		boolean con_1 = str.matches("^{2,}[a-zA-Z]*$");
		boolean con_2 = str.matches("^{1,}[0-9]*$");
		boolean con_3 = str.matches("^[-!@$]*$");
		boolean con_4 = matcher.matches();
		if(!con_1 && !con_2 && !con_3 && ! con_4) {
			System.out.println("Weak");
		}else if((con_1 || con_2) && (con_2 || con_3) && (con_3 || con_4) && (con_4 || con_1)) {
			System.out.println("Good");
		}else if(con_1 && con_2 && con_3 && con_4) {
			System.out.println("Strong");
		}else if(con_1 || con_2 || con_3 || con_4) {
			System.out.println("Moderate");
		}
	}
}
