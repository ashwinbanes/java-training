
public class Pattern {
	Pattern() {
	}
	public static void main(String[] args) {
		printPat();
	}
	public static void printPat() {
		int n = 3, stars = 1;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i; j++)
                System.out.print(j);
                stars++;
            for (int k = stars; k <= 7; k++)
                System.out.print("*");
            System.out.println();
        }
        System.out.println();
	}
}
